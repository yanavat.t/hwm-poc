import { StarIcon } from "@chakra-ui/icons";
import {
  AspectRatio,
  Badge,
  Box,
  Center,
  Container,
  Divider,
  Grid,
  Stack,
  Text,
} from "@chakra-ui/layout";

import { FormControl, FormLabel, Switch, useBoolean } from "@chakra-ui/react";

import {
  Button,
  Image,
  Spinner,
  Table,
  Tbody,
  Td,
  Tfoot,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { Formik, Form } from "formik";
import React from "react";
import { Line } from "react-chartjs-2";
import { DarkModeSwitch } from "../components/DarkModeSwitch";
import { InputField } from "../components/InputField";
import { Wrapper } from "../components/Warpper";

interface Props {}

type FundDataType = {
  period: number;
  portValue: number;
  fee: number;
  valAfterFee: number;
  highWaterMark: number;
  withdrawFund: number;
  isTrigger: boolean;
};
const initalFund = 100;
const feePercent = 30;
const HWMPercent = 10;
const initFundData = [
  {
    period: 0,
    portValue: initalFund,
    fee: 0,
    valAfterFee: 0,
    highWaterMark: parseFloat((initalFund * (1 + 10 / 100)).toFixed(3)),
    withdrawFund: 0,
    isTrigger: false,
  },
];

const wowfund = (props: Props) => {
  const [fundData, setFundData] = React.useState<FundDataType[]>(initFundData);
  const [flag, setFlag] = useBoolean();
  const [complete, setComplete] = React.useState(true);

  const calculateHWM = (valAfterFee: number) => {
    return valAfterFee * (1 + HWMPercent / 100);
  };
  const isTrigger = (currentPortvalue: number, highWaterMark: number) =>
    currentPortvalue >= highWaterMark;
  const calculateFee = (
    currentPortvalue: number,
    prevfundData: FundDataType
  ) => {
    const newFee =
      (currentPortvalue - prevfundData.portValue) * (feePercent / 100);

    return isTrigger(currentPortvalue, prevfundData.highWaterMark) ? newFee : 0;
  };
  const randomFundData = (
    prevFundData: FundDataType,
    index?: number
  ): FundDataType => {
    const newPv = parseFloat(
      (
        prevFundData.portValue +
        ((Math.random() * 13.0) / 100) * prevFundData.portValue
      ).toFixed(3)
    );
    const newFundWithAdj = parseFloat(
      (newPv - prevFundData.withdrawFund).toFixed(3)
    );

    const newFee = parseFloat(
      calculateFee(newFundWithAdj, prevFundData).toFixed(3)
    );

    const newValAfterFee = parseFloat((newFundWithAdj - newFee).toFixed(3));
    const newHWM =
      newFee === 0 && prevFundData.withdrawFund === 0
        ? prevFundData.highWaterMark
        : parseFloat(calculateHWM(newValAfterFee).toFixed(3));
    const period = index ? index : prevFundData.period + 1;

    const newItem = {
      period: period,
      portValue: newFundWithAdj,
      fee: newFee,
      valAfterFee: newValAfterFee,
      highWaterMark: newHWM,
      withdrawFund:
        (prevFundData.period + 1) % 3 === 0
          ? parseFloat(
              (((newPv * 10) / 100) * (Math.random() * 5.0)).toFixed(3)
            )
          : 0,
      isTrigger: isTrigger(newPv, prevFundData.highWaterMark) && newFee > 0,
    };
    return newItem; //setFundData(newFund);
  };

  const data = {
    labels: fundData.map((item) => item.period),
    datasets: [
      {
        label: "# of Pv",
        data: fundData.map((item) => item.portValue),
        fill: false,
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgba(255, 99, 132, 0.2)",
        yAxisID: "y-axis-1",
      },
      {
        label: "# of HWM",
        data: fundData.map((item) => item.highWaterMark),
        fill: false,
        backgroundColor: "rgb(54, 162, 235)",
        borderColor: "rgba(54, 162, 235, 0.2)",
        yAxisID: "y-axis-2",
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          type: "linear",
          display: true,
          position: "left",
          id: "y-axis-1",
        },
        {
          type: "linear",
          display: true,
          position: "right",
          id: "y-axis-2",
          gridLines: {
            drawOnArea: false,
          },
        },
      ],
    },
  };
  return (
    <Container height="100vh">
      {/* <Center h="300px"> */}

      <Wrapper variant="small">
        <Text color="teal">{`Inital Fund : ${initalFund}`}</Text>
        <Text color="teal">{`Fee Percent :${feePercent}`}</Text>
        <Text color="teal">{`HWM Percent :${HWMPercent}`}</Text>
        <Grid templateColumns="repeat(1, 1fr)" gap={2}>
          <Box w="100%">
            <Line data={data} options={options} />
            <Formik
              initialValues={{ email: "" }}
              onSubmit={(values) => {
                console.log("submit");

                if (!flag) {
                  setTimeout(() => {
                    const newData = randomFundData(
                      fundData[fundData.length - 1]
                    );
                    setFundData([...fundData, newData]);
                    setComplete(true);
                  }, 200);
                } else {
                  let tempFundData = fundData.slice(0);
                  for (let i = 0; i <= 11; i++) {
                    //   setTimeout(() => {
                    const newData = randomFundData(
                      fundData[fundData.length - 1],
                      tempFundData.length
                    );
                    // console.log(newData);
                    tempFundData.push(newData);

                    // setFundData([...fundData, newData]);
                    // }, 1000);
                  }
                  console.log(tempFundData);

                  setFundData(tempFundData);

                  setComplete(true);
                }
              }}
            >
              {({ isSubmitting }) =>
                !complete ? (
                  <Spinner colorScheme="red.500" />
                ) : (
                  <Form>
                    {/* <InputField
                  name="portvalue"
                  placeholder="portvalue"
                  label="Portvalue"
                  type="number"
                /> */}
                    <Stack align="center" direction="row" mx="4" mt="4">
                      <Button
                        type="submit"
                        isLoading={!complete}
                        colorScheme="teal"
                      >
                        Add data
                      </Button>
                      <Button
                        // type="submit"
                        isLoading={!complete}
                        colorScheme="red"
                        onClick={() => {
                          setFundData(initFundData);
                        }}
                      >
                        reset
                      </Button>
                      <Box>
                        {/* <p>Boolean state: {flag.toString()}</p> */}
                        <FormControl display="flex" alignItems="center">
                          <Switch
                            id="email-alerts"
                            onChange={() => {
                              setFlag.toggle();
                            }}
                          />
                          <FormLabel
                            mx="4"
                            htmlFor="email-alerts"
                            color="blue.600"
                          >
                            Brust Mode ?
                          </FormLabel>
                        </FormControl>
                      </Box>
                    </Stack>
                  </Form>
                )
              }
            </Formik>
          </Box>
          <Box w="100%">
            <Table size="sm">
              <Thead>
                <Tr>
                  <Th isNumeric>period</Th>
                  <Th isNumeric>Portvalue</Th>
                  <Th isNumeric>Changed%</Th>
                  <Th isNumeric>Fee</Th>
                  <Th isNumeric>VAF</Th>
                  <Th isNumeric>HWM</Th>
                  <Th isNumeric>Withdraw</Th>
                  <Th>Trigger</Th>
                </Tr>
              </Thead>
              <Tbody>
                {fundData.length > 0 &&
                  fundData.map((item, index) => (
                    <Tr key={index}>
                      <Td>{item.period}</Td>
                      <Td>{item.portValue} </Td>
                      <Td>
                        {index > 0
                          ? `${parseFloat(
                              (
                                ((item.portValue -
                                  fundData[index - 1].portValue) *
                                  100) /
                                fundData[index - 1].portValue
                              ).toFixed(3)
                            )}`
                          : 0}
                      </Td>
                      <Td>{item.fee}</Td>
                      <Td>{item.valAfterFee}</Td>
                      <Td>{item.highWaterMark}</Td>
                      <Td>
                        {item.withdrawFund !== 0 ? (
                          <Badge variant="solid" colorScheme="red">
                            {item.withdrawFund}
                          </Badge>
                        ) : (
                          0
                        )}
                      </Td>
                      <Td>
                        {item.isTrigger ? (
                          <Badge variant="solid" colorScheme="green">
                            triggered
                          </Badge>
                        ) : (
                          <Badge>nah</Badge>
                        )}
                      </Td>
                    </Tr>
                  ))}
              </Tbody>
            </Table>
          </Box>
        </Grid>
      </Wrapper>
      <DarkModeSwitch />
      {/* </Center> */}
    </Container>
  );
};
export default wowfund;
